# README #

MTCNN implementation for face recognition

## Training on own images classifier 

1. Create folder for training image(I named it train), having images of each person/class in dedicated subfolders
2. Create folder for targeted aligned images (I named it aligned)
3. run align_dataset_mtcnn.py file
python align_dataset_mtcnn.py train/ aligned/ --image_size 182 --margin 44 (input to the Inception-ResNet-v1 model is 160x160 pixels giving some margin to use a random crop ).
This will give aligned images with bboxes in a text file
4. run classifier.py file to train on given inception protobuf file
python classifier.py TRAIN aligned/ models/20170512-110547.pb output_pkl/my_classifier.pkl --batch_size 128 --min_nrof_images_per_class 40 --nrof_train_images_per_class 35 --use_split_dataset
This will give the classifier file.
5. now open face.py and add the paths to the pb and classifier file
6. Lastly run real_time_face_recognition.py file
7. Test if its ok


## Training on scratch using Inception Resnet model
1. Create folder for training image(I named it train), having images of each person/class in dedicated subfolders
2. Create folder for targeted aligned images (I named it aligned)
3. run align_dataset_mtcnn.py file
python align_dataset_mtcnn.py train/ aligned/ --image_size 182 --margin 44 (input to the Inception-ResNet-v1 model is 160x160 pixels giving some margin to use a random crop ). This will give aligned images with bboxes in a text file
5. Now train it using softmax or triplet loss given in facenet papers. I have done softmax loss.
python train_softmax.py --logs_base_dir path to logs folder --models_base_dir path to dave checkpoints --data_dir path to aligned image using mtcnn --image_size 160 --model_def models.inception_resnet_v1 --optimizer RMSPROP --learning_rate -1 --max_nrof_epochs 80 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-2 --center_loss_alfa 0.9
6. Check for tensorboard log file 
